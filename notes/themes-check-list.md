# Check-list intégration graphiques

## Personnalisée

### Typos

- Utiliser plus de deux fontes différentes est une mauvaise pratique.
- Pas de fontes non-libres.
- Respecter les contrastes (https://colourcontrast.cc/fae3e1/e9605d).

### Général

- Déconseiller l'utilisation de liens vers les réseaux sociaux.
- N'utiliser que les layouts existants.
- Pas de version tablette, uniquement desktop / mobile.
- Pas de widget, HTML ou JS externe.
- Pas de largeur de cellules personnalisées : coller à la grid, 1/2, 1/3 ou 1/5.

### Navigation

- Le menu burger ne peut pas contenir d'autres éléments que la navigation.
- Les éléments des cellules listes de lien ne peuvent pas avoir d'icône[^1].
- Uniquement la nav, logo et liens de connexion dans le header.
- Pas de fil d'Ariane.

### Combo

- L'emplacement des éléments composant un champ n'est pas modifiable.

### WCS

- Couleur, fond ou autre personalisation des items de liste. -> Demanderai à utiliser select2 pour les select standards.

## Standard

### Général
- Pas d'icône dans les inputs (cellule recherche par ex.).

### Combo
- L'image d'une cellule liste de liens est en dessous du titre.

### WCS
- - Pas de positionnement des labels horizontallement par rapport aux champs

### Navigation

- La flèche pour déplier / replier une catégorie est toujours sur le titre
- Pas de changement d'intitulé des boutons connexion / inscription Combo.
- Textes non personnalisables:

## Native :


[^1]: https://dev.entrouvert.org/issues/68575
