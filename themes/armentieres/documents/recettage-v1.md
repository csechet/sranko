##### 1. Header & Navigation

## En version Desktop :

[X] Le logo du header est légèrement trop grand, il n’y a pas assez de marges
    en haut et en bas. Il ne devrait pas dépasser 190px de large. Ici il fait
    environ 250px.
   
[~] Le font-size de la navigation est légèrement trop grand (peut être descendre
    à 15px). Les boutons «Connexion» et «Inscription» passent à la ligne
    suivante quand il y a plus de 4 entrées dans la navigation.
    > Que faire quand il n'y a plus assez de place pour afficher le menu ?

 X L’effet au survol des items de la navigation devrait être :
   border-bottom: solid 4px #eb8772; et color: #333333. Actuellement il n’y a
   pas de changement de couleur du texte au survol (effet qui doit rester actif
   au clic) et l’effet de bordure n’est que de 3px au lieu de 4px. Si changer
   la couleur du texte au survol n’est pas possible, alors passer le texte en
   #333333.

 X L’effet au survol des items de la navigation doit être identique dans le
   menu déroulant. Cependant, le border-bottom fonctionne beaucoup moins bien
   visuellement dans le volet déroulant.

 X Il faudrait réduire l’interlignage du volet déroulant, l’espace vertical
   entre les items est beaucoup trop grand.

 X Le font-size des boutons «Connexion» & «Inscription» est légèrement trop
   grand (peut être le réduire à 15px).

--- En version Mobile :

 X Faire un menu correspondant au guide de style.
 
 X Le logo du header est légèrement trop grand, il n’y a pas assez de marges
   en haut et en bas. Il ne devrait pas dépasser 160px de large. Ici il fait
   toujours environ 250px.

 X Les boutons «Connexion» & «Inscription» ne sont pas placés dans le menu
   hamburger. Si c’est impossible, il faudrait au moins les centrer dans la
   page.
   ***** NOK pour le menu hamburger

 X La navigation hamburger, une fois ouverte, devrait prendre 100% de la
   largeur de l’écran.

 X Retirer le scroll horizontal qui s’est créé.

 X Il y a un bug avec le bouton «X» pour fermer la navigation : le bouton
   scroll avec le reste de la page au lieu de rester fixe en haut du menu.
   De plus, selon le format de l’écran, le bouton ne se place pas correctement
   horizontalement.

 X Se référer aux guide des styles (p14) pour adopter le graphisme du menu
   déployé
   
 X Marge droite du logo en mobile
 
 X Toplinks qui wrappent

##### 2. Bandeau & Carrousel

 X Le bandeau n’a pas la bonne hauteur. Il devrait mesurer 290px, ici il en
   fait 338px.

 • La phrase d’accorche «Bienvenue sur votre portail...» est légèrement
   trop grande.

 X Il y a un problème avec l’incrustation de l’image de fond (les couleurs ne
   correspondent pas).

 X Lorsque que le carrousel defile, il doit prendre tout l’espace du bandeau.
   (Au pire avec des marges blanches sur les côtés).

 X Il faudrait augmenter la taille du titre de l’actualité. Et le ferrer à
   gauche en laissant tout de même une belle marge pour qu’il ne soit pas collé
   au bord.

 ~ Les flèches de défilement ainsi que les boutons de navigation doivent être
   en blanc. Le bouton de navigation de la slide active doit être rempli en
   #eb8772 avec un contour blanc (voir guide des styles).
    > Flèches de navigation ou pas ?

##### 3. Barre latérale

## a. Outil de recherche

 • Problème d’affichage des résultats de recherche : remontée des mots
   suivants : recherche/ accueil / recherche / accueil / mentions légales /
   aide numérique ... Il semble y avoir un bug (Tests réalisés avec le mot
   signalement + le mot numérique)

 X Il manque le texte qui est censé accompagner la barre de recherche.

 X Il manque le pictogramme de loupe avant la mention «Recherche». Si ce n’es
   pas possible, le mettre à l’extérieur de la barre de recherche, à gauche.
 
 X En version mobile, la barre de recherche est censé se retrouver juste en
   dessous du «header» et juste avant les blocs «code se suivi» et «signalement».
   Est ce possible ? Si non, est-il possible de l’afficher dans le menu hamburger ?
   ***** NOK Pour le menu hamburger

 X Ajouter un trait horizontal léger (#939393) pour séparer les résultats de la
  recherche.

## b. Vous êtes & Liens utiles
 X Réduire la taille de la police

 X Réduire les espaces d’interlignage

 X Ouvrir les liens utiles dans un nouvel onglet
   ***** NOK

 X Il n’était pas prévu d’effet de surlignement au survol des liens de ces
   listes. Mais peut-être faut-il ajouter un effet tout de même.
   (color : #939393 vers color : #333333 ?)

 X Les items des listes doivent être en couleur #333333.

##### 4. Code de suivi & Signalement
--- En version Desktop :

 • Le bloc «signalement» n’a pas la même hauteur que le bloc «code de suivi».
   La hauteur des blocs ne devrait pas dépasser 170px. Ici elle fait au
   maximum 280px.

 • Il n’y a pas les pictogrammes qui doivent accompagner les blocs. À savoir,
   le cadena pour le bloc «code de suivi» et le pin pour le bloc «signalement».
   ***** NOK Pour le cadenas 

 • Sur Chrome, la couleur de fond du bloc «code de suivi» ne s’affiche pas.

 • Le champ de saisie du code de suivi ne correspond pas en terme de styles
   et de dimensions. Il devrait mesurer au maximum 278px de large et toujours
   mesurer 35px de hauteur. Ici il mesure 367px / 44px. Il ne doit pas y avoir
   de contour sur le champ de saisie ni d’effet au survol (ombre portée).
   
 • Le bouton «valider» qui accompagne le champ de saisi du code de suivi est
   plus grand (en hauteur) que le dit champ de saisie. Il devrait lui aussi
   mesurer 35px de hauteur. La couleur du texte de ce bouton devrait être
   «blanc» et non pas #eb8772.

 • Les border-radius du champ de saisie et du bouton «valider» ne doivent pas
   s’appliquer là où ils se touchent. Pour le champ de saisie, les border-radius
   se trouvent à gauche tandis que pour le bouton «valider», les border-radius se
   trouvent à droite.

 • Dans le bloc «signalement», le texte ne doit pas être justifié.

 • Le bouton «signaler» du bloc «signalement» doit faire la même hauteur que le
   bouon «valider» du bloc «code de suivi», à savoir 35px.

 • Le texte des bouton «valider» et «signaler» doit être de la même taille.

 • Réduire légèrement la aille de la police.

 • Modifier le titre et la phrase du code suivi :
 
    Titre : VOTRE CODE DE SUIVI
    Phrase : « Un code de suivi est associé à chaque demande afin de faciliter vos
    échanges avec les services de la Mairie.
    Pour retrouver votre demande via le code de suivi, indiquez-le ci-dessous : »

--- En version Mobile :

 • Le titre «code de suivi» doit être centré.

 • Le bouton «valider» associé au champ à remplir doit passer en dessous du champ.

 • Dans cette version, le bouton «valider» et le champ à remplir doivent mesurer 279px
   de large et 45px de haut.

 • Le bouton «signaler» doit être centré dans le bloc «signalement» et doit lui aussi
   mesurer 279px/45px.

 • Aligner le texte du bloc «signalement» avec celui du bloc «code de suivi» et
   ferrer à gauche.

##### 5. Démarches & Catégories de démarches

 • Les blocs de catégories de démarches doivent être fermés par défaut au
   chargement de la page.

 • La bordure des blocs doit être de couleur #b3b3b3 et pas #919191.

 • Les démarches doivent (si possible) s’afficher également dans un bloc
   avec un border : solid 1px #b3b3b3. Le bloc doit mesurer au maximum 764px de
   large et faire une hauteur de 50px. Il doit être centré sous la catégorie de
   démarche à laquelle il appartient.

 • S’il n’est pas possible de mettre les démarches dans des blocs sous les
   catégories, il faudrait au moins les séparer par une ligne horizontale de
   couleur #b3b3b3.

 • Masquer les descriptions de démarches depuis la page d’accueil desktop et
   mobile

 • Lorsqu’un formulaire est un lien externe, ‘external-link’ est bien indiqué
   dans la CSS mais cela n’indique pas le pictogramme associé.
   (J’ai également attribué la CSS pour les liens utiles sur la partie barre
   latérale et cela ne fonctionne pas non plus.)
   
 • Lorsqu’un formulaire nécessite une authentification : le pictogramme verrou
   n’est pas le bon, une clé est indiqué à la place du cadenas.

 • Les pictogrammes des catégories de démarches sont coupés et leur emplacement
   n’est pas centré horizontalement.

 • Le pictogramme «flèche» des catégories doit avoir une marge à droite de 30px.

 • En version mobile, les pictogrammes des démarches doivent avoir une marge à
   droite de 20px tout comme la «flèche» d’ouverture des catégories de
   démarches.

 • Les titres h2 sont instables, un fois rouge, une fois noir.

 • Pour les paragraphes ‘information’ : réduire la marge haute et basse de la
   zone.

 • Réduire la taille de la police ‘date’ dans la présentation de l’agenda.

 • Liste à puces : interlignage des lignes un peu trop grand

 • Les titres des formulaires doivent être en H2 majuscule et #eb8772.

 • Dans les pages de formulaires, les pictogrammes associés aux bandeau de
   messages ne sont pas centrés verticalement.

 • Les flèches pour selectionner une date de rendez-vous ne sont pas centrées
   dans les boutons.

 • Les boutons précédent et suivant devraient mesurer 120px/45px. Le bouton
   abandonner devrait lui aussi mesurer 45px de hauteur.

 • Dans la page récapitulative, foncer la date et l’heure de la prise de
   rendez-vous.

 • Différencier les intitulés de page dans le résumé car il y a un problème
   de lisibiité.

 • Mettre une barre horizontale entre résumé et historique pour séparer
   visuellement ces deux possibilités.

##### 6. Proposer une démarche

 • Le bloc «proposer une démarche» doit faire 80px de hauteur et non 140px.
 
 • Le bouton «en savoir plus» devrait être ferré à droite avec un marge de
   20px. Le texte du bouton devrait être «blanc» et il devrait être accompagné
   par une fléche pointant vers la droite. Le bouton devrait mesurer au
   maximum, 140px de largeur et 40px de hauteur. Ici il est légèrement plus
   grand, 154px/42px.

 • En version mobile, le bloc était censé changer légèrement d’apparence
   (à voir si c’est faisable). Sinon voir pour adapter la version desktop à
   la version mobile, ou inversement (voir guide des styles).

 • En version mobile, il serait préférable que tout le bloc soit cliquable,
   plutôt que juste un bouton

##### 7. Footer

 • Le footer devrait mesurer 305px de hauteur au maximum, ici il fait 350px.

 • Les 3 blocs de textes de coordonnées ne sont pas centrés dans le footer.
   Ils sont décalés vers la droite. Mais c’est probablement car il manque le
   logo «Mes démarches Armentieres.fr» ainsi que les pictogrammes des réseaux
   sociaux ?

 • En version mobile, il manque aussi le logo «Mes démarches Armentieres.fr»
   ainsi que les pictogrammes des réseaux sociaux qui devraient se trouver au
   dessus des textes de coordonnées.

 • Les logos ne sont pas centrés, ils sont décalés vers la gauche.

 • Les logos en version desktop ne doivent pas dépasser 60px de hauteur. Ici il
   font +/- 100px.
 
 • Les logos doivent avoir le même espacement entre eux.

 • Le second footer qui se trouve en dessous du premier devrait contenir une
   série de liens qui ne sont pas visibles pour l’instant.

 • En version mobile, le second footer devrait mesurer environ 155px de hauteur
   afin d’afficher les liens les uns en dessous des autres. Ici il a la même
   hauteur qu’en version desktop.

 • En version mobile, les logos devraient être centrés dans le footer

##### 8. Bouton «Remonter»

 • Le pictogramme de la flèche n’est pas le bon (à fournir à Entre’Ouvert).

 • Le bouton devrait être carré et mesurer 50px/50px et avoir un
   border-radius de 13px.

 • Le bouton devrait apparaitre à l’écran dès que l’utilisateur scroll
   dans la page et rester fixe en bas de cette page. Ici il est accroché
   au footer

##### 9. Global

 • L’utilisation des typographies n’est pas respectée. Tout a été remplacé
   par de la LATO. Pour rappel, les titres H1 doivent être en ARCHIVO BLACK,
   les titres de H2 à H4 en VIGA REGULAR et le corps de texte en RUBIK REGULAR.
   L’utilisation de plusieurs typos est-elle vraiment impossible ? (Sinon, se
   référer au guide des styles pour mettre les typos dans les bonnes tailles).

 • Les textes en noir doivent être en réalité en #333333 et pas en noir 100%.

 • Serait-il possible d’utiliser des transitions pour les effets au survol.
   Des transitions rapides (0.2s, 0.3s) permettraient de rendre les effets au
   survol plus naturels.

 • Globalement, dans la version mobile, il manque un peu de marges latérales.
   Il devrait y avoir environ 15px de marge de chaque côté. Ici il y a environ
   11px.

 • Pour les boutons qui sont noir, mettre le texte en blanc.
 
##### 10 Page de connection

 • Il manque le titre de la page, à savoir : «Se connecter».

 • La mention «ou» qui sépare les deux méthodes de connexion devrait être centrée
   verticalement.

 • Le bouton FranceConnect devrait se trouver au dessus du texte «Qu’est ce que
   FranceConnect». Et le bloc entier devrait être centré verticalement.

 • Il devait y avoir un bloc dédié à la création d’un nouveau compte (voir guide
   des styles) qui n’est pas présent. Est-ce impossible ?

 • Les champs à remplir «courriel» et «Mot de passe» doivent mesurer 45px de
   hauteur et apparaître en #e6e6e6 puis au clic en «blanc» avec un
   border : solid 1px #333333.

 • Le bouton «connexion» doit lui aussi mesurer 45px de hauteur et au maximum
   110px de largeur. Il doit apparaître en «blanc» avec un
   border : solid 1px #333333, puis au survol #ebbe55 sans bordure. Il ne doit
   pas avoir d’effet d’ombre portée au survol.

 • En mode hors connexion :
     - Nous avons souhaité mettre un préambule à la page pour expliquer les 2
       possibilités de création de compte.
     
     - 2 tests sont créés et leur visibilité est attribuée aux utilisateurs
       non connectés mais lorsque l’on est connecté, ils se voient toujours ...
 
     - Lorsque l’on met ces deux textes au dessus de la zone de connexion, la
       zone de connexion se met sur la 3e colonne et devient impraticable
       (tests réalisés en mettant les paragraphes dans Haut de contenu et
       contenu)
